var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var PostSchema = new Schema({
    userID: String,
    description: String,
    img: String,
    date: String
});

module.exports = mongoose.model('Post', PostSchema);
