// Import dependencies
const express = require('express');
const router = express.Router();

//Import models
var Post = require('../models/post');

    /*
    userID: String,
    description: String,
    img: String,
    date: String
    */

router.route('/')
    .post(function(req, res) {

        var post = new Post();
        post.userID = req.body.userID;
        post.description = req.body.description;
        post.img = req.body.img;
        post.date = req.body.date;

        post.save(function(err) {
            if (err)
                res.send(err);

            res.json({ message: 'Post created!' });
        });
    })
    .get(function(req, res) {
        Post.find(function(err, posts) {
            if (err)
                res.send(err);

            res.json(posts);
        }).sort({date: -1});
    });

router.route('/:id')
    .get(function(req, res) {
        Post.findById(req.params.id, function(err, post) {
            if (err)
                res.send(err);

            res.json(post);
        });
    })
    .put(function(req, res) {
        Post.findById(req.params.id, function(err, post) {
            if (err)
                res.send(err);

            post.userID = req.body.userID;
            post.description = req.body.description;
            post.img = req.body.img;
            post.date = req.body.date;

            post.save(function(err) {
                if (err)
                    res.send(err);

                res.json({ message: 'Post updated!' });
            });

        });
    })
    .delete(function(req, res) {
        Post.remove({
            _id: req.params.id
        }, function(err, post) {
            if (err)
                res.send(err);

            res.json({ message: 'Successfully deleted' });
        });
    });


module.exports = router;
