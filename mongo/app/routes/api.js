// Import dependencies
const mongoose = require('mongoose');
const express = require('express');
const router = express.Router();

// MongoDB URL from the docker-compose file
const dbHost = 'mongodb://database/mean-docker';

// Connect to mongodb
mongoose.connect(dbHost);

// verify token
router.use(function(req, res, next) {
    console.log('Verify token here.');
    next();
});

router.get('/', (req, res) => {
    res.send('InstaVUE API');
});

router.use('/posts', require('./posts'));

module.exports = router;
