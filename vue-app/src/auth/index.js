export default {

  user: {
    authenticated: false,
    name: ''
  },

  login (name) {
    localStorage.setItem('access_token', name)
    this.user.authenticated = true
    this.user.name = name
  },

  logout () {
    localStorage.removeItem('access_token')
    this.user.authenticated = false
    this.user.name = ''
  }
}
